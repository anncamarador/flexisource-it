
/**
 * READ ME:
 * I just set up the server.js where database connection and processes will be placed here.
 * This is just in preparation if incase database is involved. But currently I did not use database, yet but I will just put database connection
 * and process here for your reference.
 */




// server.js
// set up
var express = require('express');
var mysql = require('mysql');
var app = express();                  // Create app with Express
var morgan = require('morgan');          // Log requests to console //HTTP LOGGING
var bodyParser = require('body-parser');     // Pull information from POST
var methodOverride = require("method-override");  // Sim DELETE and PUT
var request = require("request");


// config
app.use(express.static(__dirname + '/public'));                       // Static file direction
app.use(morgan('combined'));                                               // Log requests to console
app.use(bodyParser.urlencoded({'extended': 'true'}));                // Parse urlencoded
app.use(bodyParser.json());                                           // Parse json
app.use(bodyParser.json({type: 'application/vnd.api+json'}));        // Parse vnd.api+json as json
app.use(methodOverride());


//Global MySQL Connection
//mysql pool
//This is were I declare the connection pool of the database this is just for example, I will just comment this for the maintime.
// var pool = mysql.createPool({
//     connectionLimit: 100, //important
//     host: "localhost",
//     port: "3309",
//     user: "root",
//     password: "password",
//     database: "sampledbname",
//     debug: false
// });


//rest route
//get user contact information
app.post("/api/getUserInfo", function (req, res) {
    getUserInfo(req, res);
});

//get user address
app.post("/api/getUserAddress", function (req, res) {
    getUserAddress(req, res);
});

//update account information
app.post("/api/updateAccount", function (req, res) {
    updateAccount(req, res);
});

//update account Address
app.post("/api/updateAccountAddress", function (req, res) {
    updateAccountAddress(req, res);
});



// application route
app.get('*', function (req, res) {
    res.sendfile('./public/index.html') // load the single static file
});


// listen
app.listen(8081);
console.log("App listening on port 8081");


/************************
 * FUNCTIONS
 */

 function getUserInfo(req, res) {


    //Im using static data here for now since we dont have database
    //Create Select query here for the user information

    var responsedata = {"data": "", "status": "", "message": ""}
    var data = {
        "LastName":"Camarador",
        "FirstName":"Mary Ann",
        "Email":"anncamarador@gmail.com",
        "Mobile":"09616634629"
    }
    responsedata = {"data": data, "status": "000", "message": "Success"}
    res.json(responsedata);

    
}

function getUserAddress(req, res) {
    var responsedata = {"data": "", "status": "", "message": ""}


    var addressarr = [
        {
            "ID":1,
            "DetailedAddress":"Naturova Water Station",
            "Region":"Visayas",
            "Province":"Cebu",
            "City":"Mandaue City",
            "Barangay":"Maguikay",
            "Postal":"6014",
            "isPrimary":true
        },
        {
            "ID":2,
            "DetailedAddress":"Zone Agbati",
            "Region":"Visayas",
            "Province":"Cebu",
            "City":"Mandaue City",
            "Barangay":"Paknaan",
            "Postal":"6014",
            "isPrimary":false
        },
    ]
   
 

    responsedata = {"data": addressarr, "status": "000", "message": "Success"}
    res.json(responsedata);
    //Im using static data here for now since we dont have database
    //Create Select query here for the user addresses
}


function updateAccount(req,res){

    var lastname = req.body.LastName;
    var fname = req.body.FirstName; 
    var data = {
        "LastName":lastname,
        "FirstName":fname,
        "Email":"anncamarador@gmail.com",
        "Mobile":"09616634629"
    }
    responsedata = {"data": data, "status": "000", "message": "Success"}
    res.json(responsedata);

      //Im using static data here for now since we dont have database
    // Create UPDATE query here


}

function updateAccountAddress(req,res){
    //UPDATE account address here, but currently im just return it as success,I just used the front end to update it.
    //UPDATE QUERY HERE
    var responsedata = {"data": "", "status": "000", "message": "Success"}
    res.json(responsedata);
}
