// public/core.js
var app = angular.module('workmonitoring', ['ngRoute', 'ngCookies','angularUtils.directives.dirPagination']);


//Setting up routes
app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when("/account", {
            templateUrl: "accounts/account.html",
            controller: "AccountController"
        })
        .otherwise("/account")

}]);



app.controller("AccountController", ['$scope', '$http', '$location', '$cookies', function ($scope, $http, $location, $cookies) {
  

    //get profile information
    $scope.getProfile = function () {

        $http.post('/api/getUserInfo')
            .success(function (data) {
                $scope.userDetails = data.data;
            })
            .error(function (data) {
                console.log('Error: ' + data);
            });
    };

    //get addresses
    $scope.getAddresses = function () {

        $http.post('/api/getUserAddress')
            .success(function (data) {
                $scope.Addresses = data.data
            })
            .error(function (data) {
                console.log('Error: ' + data);
            });
    };

    //open account to edit
    $scope.openEditProfile = function (data) {
        $scope.efirstname = data.FirstName;
        $scope.elastname = data.LastName;
    };

    //Updat account information
    $scope.updateAccount = function(){
        var data = {
            "LastName":$scope.elastname,
            "FirstName":$scope.efirstname,
            "Email":"anncamarador@gmail.com",
            "Mobile":"09616634629"
        }

        $http.post('/api/updateAccount',data)
            .success(function (response) {
                $scope.userDetails = data
                $("#updatememberModal").modal("hide");

            })
            .error(function (data) {
                console.log('Error: ' + data);
              
        });

        
    }
    
    //open address to edit
    $scope.openPrimaryAddress = function(data){
        $scope.eregion = data.Region;
        $scope.eprovince = data.Province;
        $scope.ecity = data.City;
        $scope.ebarangay = data.Barangay;
        $scope.epostal = data.Postal
        $scope.edetailedaddress= data.DetailedAddress
        $scope.eaddid = data.ID

    }

    //update selected address
    //lets just assume here, we connected to server for backend process (updating)
    $scope.updateAddress = function(){

        $http.post('/api/updateAccountAddress')
            .success(function (data) {
                var objindex = $scope.Addresses.findIndex((obj =>obj.ID == $scope.eaddid))
                console.log(objindex)
                $scope.Addresses[objindex].DetailedAddress = $scope.edetailedaddress;
                $scope.Addresses[objindex].Region = $scope.eregion;
                $scope.Addresses[objindex].Province = $scope.eprovince;
                $scope.Addresses[objindex].City = $scope.ecity;
                $scope.Addresses[objindex].Postal = $scope.epostal;
                $scope.Addresses[objindex].Barangay = $scope.ebarangay;
                $("#updateaddressModal").modal("hide");

            })
            .error(function (data) {
                console.log('Error: ' + data);
              
        });

    }
    

    $scope.getProfile();
    $scope.getAddresses();


}]);



